from flask import Flask, render_template,  send_from_directory, Response
import hashfile
#Сейчас

app = Flask(__name__)
app.config.from_object(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/files/<path:filename>")
def get_file(filename):
    hashf = hashfile.Hashfile()
    return hashf.hash_file(filename)

@app.route("/static/<path:filename>")
def download_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename, as_attachment=True)


if __name__ == '__main__':
    app.run()