# **Hashpy (python 3.7) **

## Описание структуры проекта:

- Папка  “env” - виртуальное окружение python virtualenv
- Папка “static” - папка для раздачи статический файлов ( в нашем случае - там тот файл что нам нужно раздать)
- Папка    «templates»  - файл для размещения шаблонов html
- Файл “r.txt” - список зависимостей
- Файл “config.py” - конфиг файл, в котором указываем статическую папку
- Файл “hashpy” - файл для работы с хешем
- Файл “app.py” - центральный файл запуска приложения



## Запуск

1. Перед запуском убедитесь что python 3.7 установлен, также устанавливаем утилиту виртуального окружения через менеджер пакетов pip:

   ```python
   pip3 install virtualenv
   ```

2. Далее находясь в корне проекта, активируем окружение дав команду в терминал:

   ```python
   source env/bin/activate
   ```

3. Устанавливаем нужные зависимости:

   ```python
   (env) pip3 install -r r.txt
   ```

   

4. После этого запускаем приложение:

   ```python
   python3 app.py
   ```

   

## Работа приложения:

Файл app.py:

```python
#импорт дополнительных нужных модулей
from flask import Flask, render_template,  send_from_directory, Response
import hashfile

#создание обьекта фласк
app = Flask(__name__)
app.config.from_object(__name__)

#роуты ( адреса запросов после имени домена в адресной строке)

#роут "/" - ответсвенный за отображения главной страницы, указан рендер html шаблона, он будет отображаться по адресу yourdomain.com/
@app.route("/")
def index():
    return render_template("index.html")

#Роут с запросом файла. вытаскивает имя файла из запроса типо yourdomain.com/files/yourfile.exe
#и передает в файл hashpy.py где происходит смена хеша и затем редирект на этот файл через static для загрузки
@app.route("/files/<path:filename>")
def get_file(filename):
    hashf = hashfile.Hashfile()
    return hashf.hash_file(filename)

#Этот роут на который перебрасывает предыдущий - человека для загрузки файла
#ВНИМАНИЕ - для смены хеша используйте второй роут а не этот.
@app.route("/static/<path:filename>")
def download_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename, as_attachment=True)


if __name__ == '__main__':
    app.run()
```

